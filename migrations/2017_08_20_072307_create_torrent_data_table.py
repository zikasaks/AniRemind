from orator.migrations import Migration


class CreateTorrentDataTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('torrent_data') as table:
            # table.increments('id')
            table.text('name')
            table.integer('minimum').nullable()
            table.integer('maximum').nullable()
            table.text('link')
            table.boolean('isDelete')
            table.text('id')
            table.binary('resume_data').nullable()

            table.timestamps()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop('torrent_data')
