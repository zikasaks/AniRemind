from orator.migrations import Migration


class AddTelegramIdToUsers(Migration):
    def up(self):
        """
        Run the migrations.
        """
        with self.schema.table('users') as table:
            table.text("telegram_id").nullable()
            pass

    def down(self):
        """
        Revert the migrations.
        """
        with self.schema.table('users') as table:
            pass
