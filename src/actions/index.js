import {HEADERWINDOW} from "../constants/HeaderConsts"
import {TORRPROCESSING, TORRRECEIVED, TORRSSUCCESS} from "../constants/TorrListConst"
import {SELECTTORRENT} from "../constants/Torrents"

import fetch from 'isomorphic-fetch'

export const setWindow = id => {
  return {
    type: HEADERWINDOW,
    id
  }
};

export const fetchPostsSuccess = json => {
  console.log(json);
  return {
    type: TORRSSUCCESS,
    json
  }
};


export const requestTorrData = () => {
  return {
    type: TORRPROCESSING
  }
};

export const receivedTorrData = (torrents) => {
  return {
    type: TORRRECEIVED,
    torrents
  }
};

export function fetchTorrData() {
  return function (dispatch) {
    dispatch(requestTorrData);
    return fetch('/jsd').then(
      response=> response.json())
      .then(json=>dispatch(receivedTorrData(json)))

  }
}

export const selectTorrent = id => {
  return {
    type: SELECTTORRENT,
    id
  }
};

export const pauseTorrent = (type, id) => {
    return (dispatch) => {
        dispatch(requestTorrData);
        return fetch('/' + type + '/' + id).then(
            response => dispatch(nullAction)
        )
    }
};

const nullAction = () => {
    return {
        type: undefined
    }
};