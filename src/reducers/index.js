import { combineReducers } from 'redux'
import HeaderWindow from "./HeaderWindowReducer"
import torrents from "./TorrListReducer"
import selectedTorrent from "./TorrentsReducer"

const AniRemindFront = combineReducers({
  HeaderWindow, torrents, selectedTorrent
})

export default AniRemindFront
