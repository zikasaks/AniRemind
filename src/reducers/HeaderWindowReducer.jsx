import React from 'react'
import {HEADERWINDOW} from "../constants/HeaderConsts"

const HeaderWindow = (state=0, action) =>{
  switch (action.type) {
    case HEADERWINDOW:
      return action.id;
      break;
    default:
      return state
  }
};

export default HeaderWindow
