import React from 'react'
import {TORRSSUCCESS, TORRRECEIVED, TORRPROCESSING} from "../constants/TorrListConst"

const torrents = (state={isFetching: false, items:[]}, action) => {
  switch (action.type) {
    case TORRPROCESSING:
      return Object.assign({}, state, {isFetching: true})
      break;
    case TORRRECEIVED:
      return Object.assign({}, state, {isFetching: false, items:action.torrents})
      break;
    default:
      return state
  }
}

export default torrents
