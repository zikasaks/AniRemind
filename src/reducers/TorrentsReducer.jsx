import React from "react"
import {SELECTTORRENT} from "../constants/Torrents"

const selectedTorrent = (state = null, action) => {
    switch (action.type){
        case SELECTTORRENT:
            return action.id
        default:
            return state
    }
}

export default selectedTorrent