// import 'babel-polyfill'
import React from 'react'
import {Provider} from 'react-redux'

import thunkMiddleware from 'redux-thunk'
import {applyMiddleware, createStore} from 'redux'

import {render} from 'react-dom'
import AniRemindFront from './reducers'
import App from './components/App'
import {fetchTorrData} from "./actions"

let store = createStore(
  AniRemindFront,
  applyMiddleware(thunkMiddleware)
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)


// (function(){
//     // do some stuff
//     setTimeout(arguments.callee, 60000);
// })();

(function pollServerForNewMail() {
    store.dispatch(fetchTorrData());
    setTimeout(pollServerForNewMail, 5000);
}());

// var doStuff = function () {
//   store.dispatch(fetchTorrData())
//
//    setTimeout(doStuff, 5000);
// };
// doStuff();
