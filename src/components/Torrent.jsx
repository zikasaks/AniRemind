import React from "react"
import styles from "./css/Torrent"
import TorrentControlBtns from "./TorrentControlBtns"

function getStyle(percent){
    return {
        backgroundColor:'darkgray', 
        height: 10 +'px', 
        width: + percent + "%",
        borderRadius: 5 + 'px'
    }
}

const Torrent = (props) => {
    
    return <div onClick = {props.onClick}
            className={styles.torr}>
        <div className={styles.torrent_progress}>
            <div style={getStyle(props.torrent.progress)}></div>
            <div style={{float: "right", marginTop: "0.2em"}}>{props.torrent.progress.toFixed(2) + '%'}</div>
            <div style={{float: "right", marginTop: "0.2em", marginRight: "0.2em"}}>{props.torrent.status}</div>
        </div>
            <div>{props.torrent.name}</div>
        {props.sel && <TorrentControlBtns paused={props.torrent.status == "queued"}
                                          onPauseClick={(id) => props.onChangeStateClick("pause", id)}
                                          onStopClick={(id) => props.onChangeStateClick("stop", id)}
                                          onDeleteClick={(id) => props.onChangeStateClick("delete", id)}
                                          onFullDeleteClick={(id) => props.onChangeStateClick("deleteFull", id)}/>}
    </div>
        
}

export default Torrent;