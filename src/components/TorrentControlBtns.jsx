import React from "react"
import styles from "./css/TorrentControlBtns"

export default (props) => {
    return <div className={styles.buttons}>
        <div onClick={props.onPauseClick}
             className={styles.button}>{!props.paused ? "Приостановить" : "Запустить"}</div>
        <div onClick={props.onStopClick} className={styles.button}>Остановить</div>
        <div onClick={props.onDeleteClick} className={styles.button}>Удалить</div>
        <div onClick={props.onFullDeleteClick} className={styles.button}>Удалить безвозратно</div>
    </div>
};
