import React from 'react'
import styles from './css/TorrList'
import Torrent from "./Torrent"

const TorrList = (props) => {
  return <div className={styles.body}>
    {
      props.torrents.map((torrent, index) => {
        return <Torrent onClick={() => props.onSelectTorrent(torrent.id)} 
          sel={torrent.id === props.selected}
          torrent={torrent} 
          key={index}
          onChangeStateClick = {(type) => props.onChangeStateClick(type, torrent.id)}/>
      })
    }
</div>
};

export default TorrList
