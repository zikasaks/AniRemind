import React from 'react'
import HeaderCont from '../containers/HeaderCont'
import TorrListCont from '../containers/TorrListCont'

const App = () => (
  <div>
    <HeaderCont/>
    <TorrListCont/>
  </div>
)

export default App
