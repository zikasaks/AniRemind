import { connect } from 'react-redux'
import { setWindow } from '../actions'
import Header from '../components/Header'

const mapStateToProps = (state, ownProps) => {
  return {
    id: state.HeaderWindow
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: (id) => {
      dispatch(setWindow(id))
    }
  }
}

const HeaderCont = connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)

export default HeaderCont
