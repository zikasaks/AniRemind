import {connect} from 'react-redux'
import {pauseTorrent, selectTorrent} from '../actions'
import TorrList from '../components/TorrList'

const mapStateToProps = (state, ownProps) => {
  console.log(state)
  return {
    torrents: state.torrents.items,
    selected: state.selectedTorrent
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onSelectTorrent: id => {
      dispatch(selectTorrent(id))
    },
    onChangeStateClick: (type, id) => {
      console.log(type, id)
        dispatch(pauseTorrent(type, id))
    }
  }
}

const TorrListCont = connect(
  mapStateToProps,
  mapDispatchToProps
)(TorrList)


export default TorrListCont
