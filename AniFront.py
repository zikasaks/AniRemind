import json
import os

from klein import Klein
from orator import Model, DatabaseManager

from twisted.web.static import File

from models.torrent_datum import TorrentData
from torrent.commands.SaveCommand import SaveCommand, FindByIdCommand
from torrent.parts.Notifier import Notifier
from torrent.parts.event_manager import eventManager
from dbConfig import DATABASES
from time import sleep

from torrent.part_manager import partsManager
from torrent.parts.alert_manager import alertManager
from torrent.parts.torrent_manager import torrentManager

app = Klein()


@app.route('/', branch=True)
def hello_world(s):
    a = File(os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates/index.html"))
    a.isLeaf = True
    return a


@app.route('/static/<path>', branch=True)
def static(request, path):
    print(path)
    fl = File(os.path.join(os.path.dirname(os.path.abspath(__file__)), "static/" + path))
    fl.isLeaf = True
    return fl


def configure():
    partsManager.add_part("alertManager", alertManager)
    partsManager.get_part("alertManager").start()
    partsManager.add_part('eventManager', eventManager)
    partsManager.get_part('eventManager').start()
    partsManager.add_part("torrentManager", torrentManager)
    partsManager.get("torrentManager").start()
    partsManager.add_part("Notifier", Notifier())


db = DatabaseManager(DATABASES)

Model.set_connection_resolver(db)
configure()

tor = partsManager.get_part("torrentManager")


@app.route('/jsd')
def js(request):
    return json.dumps(partsManager.get_part("torrentManager").get_all_torrents())


@app.route("/delete/<id>")
def delete(request, id):
    tor.remove_torrent(id, 0)


@app.route("/deleteFull/<id>")
def delete_fully(request, id):
    print(id)
    partsManager.get_part("eventManager").handle(FindByIdCommand(id, TorrentData), callback=_delete_fully)


def _delete_fully(res):
    if res is None: return
    res.isDelete = True

    partsManager.get_part("eventManager").handle(SaveCommand(res), callback=None)
    tor.remove_torrent(res.id)


@app.route("/pause/<id>")
def stop(request, id):
    tor.stop_torrent(id)


app.run("0.0.0.0", 8080)

while True:
    partsManager.update()
    sleep(1)
