import re

import requests

from models.torrent_datum import TorrentData


class Parser:
    def get(self):
        request_result = requests.get("http://www.anilibria.tv/api/api.php?PAGEN_1=1").json()
        res = map(self.parse, request_result['items'])
        return list(res)

    def parse(self, data):
        new = TorrentData()
        new.name = data['title']
        minimum = None
        maximum = None
        try:
            groups = re.search('(\d+)(?:\(\d+\))?(?:-(\d+)(?:\(\d+\))?)?', data['episode']).groups()
            minimum = int(groups[0])
            maximum = int(groups[1])
        except AttributeError as ex:
            pass
        except TypeError:
            if maximum is None:
                maximum = minimum
        new.minimum = minimum
        new.maximum = maximum
        new.link = 'http://anilibria.tv/' + data['torrent_link']
        new.isDelete = False
        return new
