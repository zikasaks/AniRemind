import urllib
from urllib import request

import libtorrent as lt
from orator.orm import Builder
from twisted.internet.task import LoopingCall

import config
from models.torrent_datum import TorrentData
from torrent.commands.SaveCommand import WhereCommand, WhereNotNullCommand, GetCommand, SaveCommand, FindByIdCommand
from torrent.part import Part
from torrent.part_manager import partsManager


class TorrentManager(Part):
    STATUSES = ['queued', 'checking', 'downloading metadata',
                'downloading', 'finished', 'seeding', 'allocating', 'checking fastresume']

    def __init__(self, save_path):
        """
        Create torrent downloader

        :param save_path: default path to save torrents
        """
        self.torrent = lt.session()
        self.torrent.set_alert_mask(lt.alert.category_t.all_categories)
        self.torrent.listen_on(51255, 51255)
        self.torrent_list = {}
        self._component_timer = LoopingCall(self.save_resume_data)
        self.save_path = save_path

    def start(self):
        alert_mgr = partsManager.get("alertManager")
        alert_mgr.add_handler("torrent_finished_alert", on_torrent_finished_handler)
        alert_mgr.add_handler("save_resume_data_alert", on_save_resume_data_handler)
        self._load_tors()
        self._component_timer.start(600);

    def _load_tors(self):
        cmd = WhereCommand('isDelete', '=', False, obj=TorrentData).next(
            WhereNotNullCommand('resume_data').next(GetCommand())
        )
        partsManager.get_part("eventManager").handle(cmd, self._proceed_loaded)

    def _proceed_loaded(self, res):
        for i in res:
            resume_data = lt.bdecode(i.resume_data)
            self.add_new_torrent(i, resume_data)

    import urllib.request

    def add_new_torrent(self, torrent_data, save_path=None, resume_data=None):
        """
        Add new torrent to download

        :param torrent_data: torrent object to download
        :param save_path: where torrent should be saved
        :param resume_data:
        """

        if save_path is not 'str': save_path = self.save_path
        try:
            req = urllib.request.Request(
                torrent_data.link,
                data=None,
                headers={
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
                }
            )
            f = urllib.request.urlopen(req)
            e = lt.bdecode(f.read())
        except Exception as ex:
            return 
        info = lt.torrent_info(e)
        h = self.torrent.add_torrent(
            {'ti': info, 'save_path': save_path, 'resume_data': str(resume_data)})
        id = str(h.info_hash())
        self.torrent_list[id] = {
            'name': torrent_data.name,
            'torrent': h
        }
        torrent_data.id = id
        ev = partsManager.get_part('eventManager')
        ev.handle(SaveCommand(torrent_data), callback=None)

    def get_all_torrents(self):
        """
        Get information of all current added torrents

        :return: metadata of each added torrent
        """

        metadata = []

        for key, value in self.torrent_list.items():
            s = value['torrent'].status()
            dt = {
                "id": key,
                "name": value['name'],
                "progress": s.progress * 100,
                "status": self.STATUSES[s.state]
            }
            metadata.append(dt)
        return metadata

    def get_torrent(self, torrent_id):
        """

        :param torrent_id: id of torrent for getting metadata
        :return: metadata of torrent

        :raise KeyError if the torrent with torrent_id doesn't exists
        """

        torrent = self.torrent_list[torrent_id]
        return {
            "id": torrent_id,
            "name": torrent.name(),
            "progress": torrent.progress * 100,
            "status": self.STATUSES[torrent.state]
        }

    def remove_torrent(self, torrent_id, delete_files=1):
        """

        :param torrent_id: id of torrent for delete
        :return:
        """
        try:
            self.torrent.remove_torrent(self.torrent_list[torrent_id]['torrent'], delete_files)
            del self.torrent_list[torrent_id]
        except KeyError:
            pass

    def save_resume_data(self):
        for i in self.torrent_list.keys():
            self._save_resume_data(i)

    def _save_resume_data(self, id):
        try:
            self.torrent_list[id]['torrent'].save_resume_data()
        except KeyError:
            pass

    def get_alert(self):
        return self.torrent.pop_alert()

    def stop_torrent(self, id):
        try:
            torrent = self.torrent_list[id]
            torrent['torrent'].pause()
        except KeyError as ex:
            pass


# handlers

def on_save_resume_data_handler(alert):
    def proceed(torrent):
        if torrent is None: return
        torrent.resume_data = lt.bencode(alert.resume_data)
        partsManager.get_part('eventManager').handle(SaveCommand(torrent), callback=None)

    id = str(alert.handle.info_hash())
    partsManager.get_part("eventManager").handle(FindByIdCommand(id, TorrentData), callback=proceed)


def on_torrent_finished_handler(alert):
    id = str(alert.handle.info_hash())
    torMgr = partsManager.get("torrentManager")
    torMgr._save_resume_data(id)
    notifier = partsManager.get("Notifier")
    message = notifier.con(id)
    notifier.send(message)


def on_torrent_added_handler(alert):
    id = str(alert.handle.info_hash())
    torMgr = partsManager.get("torrentManager")
    torMgr.torrents_list[id] = alert.handle


torrentManager = TorrentManager(config.CONFIG['save_path'])
