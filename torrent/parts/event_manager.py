from queue import Queue

from twisted.internet.task import LoopingCall

from torrent.part import Part


class _event_manager(Part):
    def __init__(self):
        self.handlers = {}
        self.queue = Queue()
        self.call = LoopingCall(self.hand)

    def start(self):
        self.call.start(0.2).addErrback(print)

    def handle(self, command, callback):
        self.queue.put((command, callback))

    def hand(self):
        try:
            command, callback = self.queue.get_nowait()
        except:
            return
        res = None
        if command is None: return
        while True:
            res = command.execute()

            if command.further is None:
                break
            command = command.further
            command.object = res
        if callback is not None:
            callback(res)


eventManager = _event_manager()

