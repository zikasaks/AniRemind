from twisted.internet.task import LoopingCall

from torrent.part import Part
from torrent.part_manager import partsManager


class AlertManager(Part):
    def __init__(self):
        # self.part_manager = part_manager
        self._component_timer = LoopingCall(self.handle_alerts)
        self.handlers = {}
        self.state = "stoped"

    def start(self):
        self.state = "started"
        self._component_timer.start(1).addErrback(self.err)

    def err(self, s):
        print(s)

    def stop(self):
        self._component_timer.stop()

    def update(self):
        if self.state == "stoped":
            self._component_timer.start(1)

    def handle_alerts(self):
        tor = partsManager.get("torrentManager")
        if tor is None: return
        alert = tor.get_alert()
        while alert is not None:
            alert_type = type(alert).__name__
            if alert_type in self.handlers:
                for handler in self.handlers[alert_type]:
                    handler(alert)
 #           print(type(alert).__name__)
            alert = tor.get_alert()

    def add_handler(self, alert_name, handler):

        if alert_name not in self.handlers:
            self.handlers[alert_name] = []
        self.handlers[alert_name].append(handler)


# reactor.run()
alertManager = AlertManager()
#'e084c8b6f960dad7a7abb92e94fb34fa9ea5b402'
