from functools import reduce

from telegram.ext import CommandHandler
from telegram.ext import Updater

import config
from models.torrent_datum import TorrentData
from models.user import Users
from torrent.Parser import Parser
from torrent.commands.SaveCommand import WhereCommand, GetCommand, SaveCommand, DeleteCommand, FirstCommand
from torrent.part import Part
from torrent.part_manager import partsManager


class Notifier(Part):
    def __init__(self):
        self.updater = Updater(token=config.CONFIG['bot_token'])
        start_handler = CommandHandler('start', self.start)
        stop_handler = CommandHandler('stop', self.stop)
        self.updater.dispatcher.add_handler(start_handler)
        self.updater.dispatcher.add_handler(stop_handler)
        self.queue = self.updater.job_queue
        self.queue.run_repeating(self.getUpdates, 10)
        self.updater.start_polling()
        # self.updater.idle()

    def send(self, message):
        for i in Users.all():
            self.updater.bot.sendMessage(chat_id=i.telegram_id, text=message, parse_mode='HTML')

    def start(self, bot, update):

        def sendHi(res):
            bot.sendMessage(chat_id=res.telegram_id, text='HELLO!')

        def add(res):
            if len(res) == 0:
                user = Users()
                user.telegram_id = update.message.chat_id
                cmd = SaveCommand(user)
                partsManager.get_part("eventManager").handle(cmd, callback=sendHi)

        cmd = WhereCommand('telegram_id', '=', update.message.chat_id, Users).next(
            GetCommand()
        )
        partsManager.get_part("eventManager").handle(cmd, callback=add)

    def stop(self, bot, update):
        try:
            cmd = WhereCommand('telegram_id', '=', update.message.chat_id, Users).next(
                DeleteCommand()
            )
            partsManager.get_part("eventManager").handle(cmd, callback=None)
        except:
            pass

    def getUpdates(self, bot, job):
        par = Parser()
        res = par.get()
        for i in res:
            self.check_info(i)

    def check_info(self, new):
        cmd = WhereCommand('name', '=', new.name, obj=TorrentData).next(
            FirstCommand()
        )
        partsManager.get_part("eventManager").handle(cmd, callback=self._check_info(new))

    def _check_info(self, new):

        def res(old):
            if old is None:
                partsManager.get_part('torrentManager').add_new_torrent(new)
                return
            if old.isDelete: return
            if old.maximum >= new.maximum:
                return
            old.minimum = old.maximum + 1
            old.maximum = new.maximum
            partsManager.get_part('torrentManager').remove_torrent(old.id)
            partsManager.get_part('torrentManager').add_new_torrent(new)

        return res

    def con(self, id):
        arg = TorrentData.find(id)
        string = "<a href=\"" + str(arg.link) + "\">" + str(arg.name) + "</a>"
        string += "\nСерии: "
        string += reduce(lambda x, y: str(x) + '-' + str(y), [arg.minimum, arg.maximum])
        return string
