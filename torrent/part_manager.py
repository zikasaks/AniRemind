# from Torrent.Parts.AlertManager import alertManager
# from Torrent.Torrent import torrentManager


class _PartsManager:
    def __init__(self):
        self.parts = {}

    def add_part(self, name, part):
        self.parts[name] = part

    def get_part(self, name):
        return self.parts[name]

    def get(self, name):
        try:
            return self.get_part(name)
        except KeyError:
            return None

    def update(self):
        for component in self.parts.values():
            component.update()

    def remove_part(self, name):
        if name not in self.parts.keys(): return
        del self.parts[name]

    def start(self, names=[]):
        for name in names:
            self.parts[name].start()

    def stop(self, names=[]):
        for name in names:
            self.parts[name].stop()


partsManager = _PartsManager()
