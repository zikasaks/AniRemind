from torrent.commands.Command import Command


class SaveCommand(Command):
    def __init__(self, obj=None):
        super().__init__(obj)

    def execute(self):
        self.object.save()
        return self.object


class FindByIdCommand(Command):
    def __init__(self, id, obj=None):
        super().__init__(obj)
        self.id = id

    def execute(self):
        return self.object.find(self.id)


class GetCommand(Command):
    def __init__(self, obj=None):
        super().__init__(obj)

    def execute(self):
        print("RFFS")
        a = self.object.get()
        return a


class WhereCommand(Command):
    def __init__(self, field, condition, what, obj=None):
        super().__init__(obj)
        self.field = field
        self.condition = condition
        self.what = what

    def execute(self):
        return self.object.where(self.field, self.condition, self.what)


class WhereNotNullCommand(Command):
    def __init__(self, field, obj=None):
        super().__init__(obj)
        self.field = field

    def execute(self):
        return self.object.where_not_null(self.field)


class DeleteCommand(Command):
    def __init__(self, obj=None):
        super().__init__(obj)

    def execute(self):
        return self.object.delete()


class FirstCommand(Command):
    def __init__(self, obj=None):
        super().__init__(obj)

    def execute(self):
        try:
            a = self.object.first_or_fail()
            return a
        except:
            return None
