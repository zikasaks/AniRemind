

class Command:

    def __init__(self, obj):
        self.object = obj
        self.further = None

    def next(self, command):
        self.further = command
        return self
